# cnchi-urlfix

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

Fixed cnchi status detection bug (online - offline)

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/installers/cnchi/patches/cnchi-urlfix.git
```

This fix uses the file extra.py located at:

```
https://gitlab.com/rebornos-team/installers/cnchi/code/new-cnchi-code
```

Here, the file is at:

```
/src/misc/extra.py
```

Line 423 must be modified:

```
urls = [('https', '1.1.1.1')]
```

NOTE: The same file as new-cnchi-code-lts could be used, since these files are the same.

